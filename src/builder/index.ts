/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Forms,
    NodeBlock,
    Slots,
    conditions,
    definition,
    each,
    editor,
    isNumber,
    isString,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { TextareaCondition } from "./condition";
import { TMode } from "../runner/mode";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:textarea", "Text (multiple lines)");
    },
})
export class Textarea extends NodeBlock {
    textareaSlot!: Slots.Text;

    @definition("string", "optional")
    prefill?: string;

    @definition("number", "optional")
    minLength?: number;

    @slots
    defineSlot(): void {
        this.textareaSlot = this.slots.static({
            type: Slots.Text,
            reference: "value",
            label: pgettext("block:textarea", "Multi-line text"),
            exchange: [
                "required",
                "alias",
                "exportable",
                "maxLength",
                "transformation",
            ],
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        this.editor.groups.settings();

        const minLength = new Forms.Numeric(
            Forms.Numeric.bind(this, "minLength", undefined)
        )
            .min(1)
            .max(this.textareaSlot.maxLength)
            .visible(isNumber(this.minLength))
            .indent(32)
            .width(75)
            .on(() => {
                maxLength.min(this.minLength || 1);
            });
        const maxLength = new Forms.Numeric(
            Forms.Numeric.bind(this.textareaSlot, "maxLength", undefined)
        )
            .min(this.minLength || 1)
            .visible(isNumber(this.textareaSlot.maxLength))
            .indent(32)
            .width(75)
            .on(() => {
                minLength.max(this.textareaSlot.maxLength);
            });

        this.editor.option({
            name: pgettext("block:textarea", "Limits"),
            form: {
                title: pgettext("block:textarea", "Limits"),
                controls: [
                    new Forms.Checkbox(
                        pgettext("block:textarea", "Minimum"),
                        isNumber(this.minLength)
                    ).on((min) => {
                        minLength.visible(min.isChecked);
                    }),
                    minLength,
                    new Forms.Checkbox(
                        pgettext("block:textarea", "Maximum"),
                        isNumber(this.textareaSlot.maxLength)
                    ).on((max) => {
                        maxLength.visible(max.isChecked);
                    }),
                    maxLength,
                ],
            },
            activated:
                isNumber(this.textareaSlot.maxLength) ||
                isNumber(this.minLength),
        });

        this.editor.transformations(this.textareaSlot);

        this.editor.groups.options();

        this.editor.option({
            name: pgettext("block:textarea", "Prefill"),
            form: {
                title: pgettext("block:textarea", "Prefill"),
                controls: [
                    new Forms.Text(
                        "multiline",
                        Forms.Text.bind(this, "prefill", undefined)
                    ).label(
                        pgettext(
                            "block:textarea",
                            "Prefill with the following text:"
                        )
                    ),
                ],
            },
            activated: isString(this.prefill),
        });

        this.editor.required(this.textareaSlot);
        this.editor.visibility();
        this.editor.alias(this.textareaSlot);
        this.editor.exportable(this.textareaSlot);
    }

    @conditions
    defineCondition(): void {
        each(
            [
                {
                    mode: "exact",
                    label: pgettext("block:textarea", "Text matches"),
                },
                {
                    mode: "not-exact",
                    label: pgettext("block:textarea", "Text does not match"),
                },
                {
                    mode: "contains",
                    label: pgettext("block:textarea", "Text contains"),
                },
                {
                    mode: "not-contains",
                    label: pgettext("block:textarea", "Text does not contain"),
                },
                {
                    mode: "starts",
                    label: pgettext("block:textarea", "Text starts with"),
                },
                {
                    mode: "ends",
                    label: pgettext("block:textarea", "Text ends with"),
                },
                {
                    mode: "defined",
                    label: pgettext("block:textarea", "Text is not empty"),
                },
                {
                    mode: "undefined",
                    label: pgettext("block:textarea", "Text is empty"),
                },
            ],
            (condition: { mode: TMode; label: string }) => {
                this.conditions.template({
                    condition: TextareaCondition,
                    label: condition.label,
                    autoOpen:
                        condition.mode !== "defined" &&
                        condition.mode !== "undefined",
                    props: {
                        slot: this.textareaSlot,
                        mode: condition.mode,
                    },
                });
            }
        );
    }
}
