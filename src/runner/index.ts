/** Dependencies */
import {
    NodeBlock,
    Slots,
    assert,
    isString,
    validator,
} from "tripetto-runner-foundation";
import "./condition";

export abstract class Textarea extends NodeBlock<{
    readonly prefill?: string;
    readonly minLength?: number;
}> {
    /** Contains the textarea slot with the value. */
    readonly textareaSlot = assert(
        this.valueOf<string, Slots.Text>("value", "static", {
            prefill:
                (isString(this.props.prefill) && {
                    value: this.props.prefill,
                }) ||
                undefined,
        })
    );

    /** Contains if the block is required. */
    readonly required = this.textareaSlot.slot.required || false;

    /** Contains the maximum text length. */
    readonly maxLength = this.textareaSlot.slot.maxLength;

    @validator
    validate(): boolean {
        return (
            !this.props.minLength ||
            !this.textareaSlot.value.length ||
            this.textareaSlot.value.length >= this.props.minLength
        );
    }
}
